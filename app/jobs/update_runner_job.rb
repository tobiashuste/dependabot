# frozen_string_literal: true

class UpdateRunnerJob < ApplicationJob
  include UpdaterContext

  queue_as :default

  sidekiq_options retry: UpdaterConfig.job_retries

  # Trigger update runner
  #
  # @param [Hash] args
  # @return [void]
  def perform(args)
    symbolized_args = args.symbolize_keys
    project_name, package_ecosystem, directory = symbolized_args
                                                 .slice(:project_name, :package_ecosystem, :directory)
                                                 .values

    run_within_context(job_details("dep-update", project_name, package_ecosystem, directory)) do
      runner_class.call(project_name, package_ecosystem, directory)
    end
  end

  private

  # Update runner class
  #
  # @return [Updater::RunnerBase]
  def runner_class
    return Updater::Compose::Runner if UpdaterConfig.compose_deployment?

    Updater::Kubernetes::Runner
  end
end
