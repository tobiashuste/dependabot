# frozen_string_literal: true

# Dependency update job
#
class DependencyUpdateJob < ApplicationJob
  include UpdaterContext

  # Don't allow enquing this job
  private_class_method :perform_later

  def perform(project_name, package_ecosystem, directory)
    @project_name = project_name
    @package_ecosystem = package_ecosystem
    @directory = directory

    run_within_context(job_details("dep-update", project_name, package_ecosystem, directory)) do
      Dependabot::UpdateService.call(
        project_name: project_name,
        package_ecosystem: package_ecosystem,
        directory: directory
      )
    end

    UpdateFailures.fetch
  rescue StandardError => e
    capture_error(e)
    raise
  end

  private

  attr_reader :project_name, :package_ecosystem, :directory

  # Update job
  #
  # @return [Update::Job]
  def update_job
    @update_job ||= Project.find_or_initialize_by(name: project_name)
                           .update_jobs
                           .find_or_initialize_by(
                             package_ecosystem: package_ecosystem,
                             directory: directory
                           )
  end

  # Dependency update run
  #
  # @return [Update::Run]
  def update_run
    @update_run ||= Update::Run.create!(job: update_job)
  end
  alias_method :create_update_run, :update_run
end

DependencyUpdateJob.prepend(ServiceMode::DependencyUpdateJob) if AppConfig.service_mode?
