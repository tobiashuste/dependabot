# frozen_string_literal: true

module Update
  # Job execution log entry
  #
  # @!attribute timestamp
  #   @return [DateTime]
  # @!attribute level
  #   @return [String]
  # @!attribute message
  #   @return [String]
  #
  class LogEntry
    include Mongoid::Document
    include Mongoid::Timestamps

    field :timestamp, type: DateTime
    field :level, type: String
    field :message, type: String

    belongs_to :run, class_name: "Update::Run"

    index({ created_at: 1 }, { expire_after_seconds: UpdaterConfig.expire_run_data })
  end
end
