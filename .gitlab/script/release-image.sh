#!/bin/bash

# Script for image release on CI
#
set -e

source "$(dirname "$0")/utils.sh"

release_version="$(echo $CI_COMMIT_TAG | grep -oP 'v\K[0-9.]+(-alpha\.\d+)?')"
ecosystems=(
  bundler
  npm
  gomod
  pip
  docker
  composer
  pub
  cargo
  nuget
  maven
  gradle
  mix
  terraform
  elm
  gitsubmodule
)

function tag_and_push() {
  destination=$1
  separator=$2

  log_info "Pushing core image"
  docker buildx imagetools create -t "$destination:$release_version" "$APP_IMAGE"
  docker buildx imagetools create -t "$destination:latest" "$APP_IMAGE"

  log_info "Pushing updater images"
  for ecosystem in "${ecosystems[@]}"; do
    local app_image="${CI_REGISTRY_IMAGE}/dev/${ecosystem}:${CURRENT_TAG}"
    local release_image="${destination}${separator}${ecosystem}"

    docker buildx imagetools create -t "$release_image:$release_version" "$app_image"
    docker buildx imagetools create -t "$release_image:latest" "$app_image"
  done
}

log_with_header "Tagging and pushing releases to dockerhub"
tag_and_push "$DOCKERHUB_IMAGE" "-"

log_with_header "Tagging and pushing release to gitlab registry"
tag_and_push "$GITLAB_IMAGE" "/"
