#/bin/bash

# Build script for image building on CI

set -e

source "$(dirname "$0")/utils.sh"

image_type=$1
base_image="ghcr.io/andrcuns/dependabot-updater-$image_type:$(dependabot_revision)"
latest_tag="${LATEST_TAG:-${CI_COMMIT_REF_SLUG}-latest}"

if [ "$image_type" == "core" ]; then
  dockerfile="Dockerfile.core"
  image="$CI_REGISTRY_IMAGE/dev"
else
  dockerfile="Dockerfile.ecosystem"
  image="$CI_REGISTRY_IMAGE/dev/$image_type"
fi

images="$image:$CURRENT_TAG,$image:$latest_tag"

log_with_header "Building image '$image:$CURRENT_TAG'"
buildctl-daemonless.sh build \
  --frontend dockerfile.v0 \
  --local context=. \
  --local dockerfile=. \
  --opt filename="$dockerfile" \
  --opt platform="$BUILD_PLATFORM" \
  --opt build-arg:COMMIT_SHA="$CI_COMMIT_SHA" \
  --opt build-arg:PROJECT_URL="$CI_PROJECT_URL" \
  --opt build-arg:VERSION="${CI_COMMIT_TAG:-$CURRENT_TAG}" \
  --opt build-arg:BASE_IMAGE="$base_image" \
  --import-cache type=registry,ref="$image:latest" \
  --import-cache type=registry,ref="$image:$latest_tag" \
  --export-cache type=inline \
  --output type=image,\"name="$images"\",push=true
