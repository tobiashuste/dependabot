# API reference

Following api endpoints are exposed by application

## Gitlab webhooks

POST `/api/hooks`

Handle following gitlab event webhooks

- `Push events` - default repository branch
- `Merge request events`
- `Comments`
- `Pipeline events`

## List projects

GET `/api/projects`

Response:

```json
[
  {
    "id": 1,
    "name": "dependabot-gitlab/dependabot",
    "forked_from_id": null,
    "webhook_id": 1,
    "web_url": "https://gitlab.com/dependabot-gitlab/dependabot",
    "config": [
      {
        "package_manager": "bundler",
        "package_ecosystem": "bundler",
        "directory": "/",
        "milestone": "0.0.1",
        "assignees": ["john_doe"],
        "reviewers": ["john_smith"],
        "approvers": ["jane_smith"],
        "custom_labels": ["dependency"],
        "open_merge_requests_limit": 10,
        "cron": "00 02 * * sun Europe/Riga",
        "branch_name_separator": "-",
        "branch_name_prefix": "dependabot",
        "allow": [
          {
            "dependency_type": "direct"
          }
        ],
        "ignore": [
          {
            "dependency_name": "rspec",
            "versions": ["3.x", "4.x"]
          },
          {
            "dependency_name": "faker",
            "update_types": ["version-update:semver-major"]
          }
        ],
        "rebase_strategy": "auto",
        "auto_merge": true,
        "versioning_strategy": "lockfile_only",
        "reject_external_code": true,
        "commit_message_options": {
          "prefix": "dep",
          "prefix_development": "bundler-dev",
          "include_scope": "scope"
        },
        "registries": [
          {
            "type": "docker_registry",
            "registry": "https://registry.hub.docker.com",
            "username": "octocat"
          }
        ]
      }
    ]
  }
]
```

## Get project

GET `/api/projects/:id`

- `id` - URL escaped full path or id of the project

Response:

```json
{
  "id": 1,
  "name": "dependabot-gitlab/dependabot",
  "forked_from_id": null,
  "webhook_id": 1,
  "web_url": "https://gitlab.com/dependabot-gitlab/dependabot",
  "config": [
    {
      "package_manager": "bundler",
      "package_ecosystem": "bundler",
      "directory": "/",
      "milestone": "0.0.1",
      "assignees": ["john_doe"],
      "reviewers": ["john_smith"],
      "approvers": ["jane_smith"],
      "custom_labels": ["dependency"],
      "open_merge_requests_limit": 10,
      "cron": "00 02 * * sun Europe/Riga",
      "branch_name_separator": "-",
      "branch_name_prefix": "dependabot",
      "allow": [
        {
          "dependency_type": "direct"
        }
      ],
      "ignore": [
        {
          "dependency_name": "rspec",
          "versions": ["3.x", "4.x"]
        },
        {
          "dependency_name": "faker",
          "update_types": ["version-update:semver-major"]
        }
      ],
      "rebase_strategy": "auto",
      "auto_merge": true,
      "versioning_strategy": "lockfile_only",
      "reject_external_code": true,
      "commit_message_options": {
        "prefix": "dep",
        "prefix_development": "bundler-dev",
        "include_scope": "scope"
      },
      "registries": [
        {
          "type": "docker_registry",
          "registry": "https://registry.hub.docker.com",
          "username": "octocat"
        }
      ]
    }
  ]
}
```

## Add project

POST `/api/projects`

Add new project or update existing one and sync jobs

- `project` - full project path
- `gitlab_access_token` - optional project specific gitlab access token

Request:

```json
{
  "project": "dependabot-gitlab/dependabot",
  "gitlab_access_token": "custom-project-access-token"
}
```

## Update project

PUT `/api/projects/:id`

Update project attributes

Request:

- `id` - URL escaped full path or id of the project
- `name` - full project path
- `forked_from_id` - id of upstream project
- `forked_from_name` - upstream project path with namespace
- `webhook_id` - webhook id
- `web_url` - project web url
- `config` - dependabot configuration array

```json
{
  "name":"name",
  "forked_from_id": 1,
  "webhook_id":1,
  "web_url": "new-url",
  "config": []
}
```

## Delete project

DELETE `/api/projects/:id`

- `id` - URL escaped full path or id of the project

## Notify release

POST `/api/notify_release`

Notifies Dependabot of dependency release. In response, Dependabot will check all projects and update the package.

- `name`: package name
- [`package-ecosystem`](https://docs.github.com/en/code-security/supply-chain-security/keeping-your-dependencies-updated-automatically/configuration-options-for-dependency-updates#package-ecosystem): value from supported ecosystem.

```json
{
  "name": "package-name",
  "package_ecosystem": "package-ecosystem"
}
```

## Healthcheck

GET `/healthcheck`

Check if application is running and responding
