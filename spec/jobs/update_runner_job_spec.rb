# frozen_string_literal: true

shared_examples "updater job" do
  subject(:job) { described_class }

  let(:project) { create(:project) }

  let(:args) do
    {
      "project_name" => project.name,
      "package_ecosystem" => "bundler",
      "directory" => "/"
    }
  end

  before do
    allow(updater_class).to receive(:call)
  end

  around do |example|
    with_env("SETTINGS__DEPLOY_MODE" => deploy_mode) { example.run }
  end

  it "queues job" do
    expect { job.perform_later(args) }.to have_enqueued_job(job)
      .with(args)
      .on_queue("default")
  end

  it "performs enqued job and saves last enqued time", :aggregate_failures do
    perform_enqueued_jobs { job.perform_later(args) }

    expect(updater_class).to have_received(:call).with(*args.values)
  end
end

describe UpdateRunnerJob, :integration, type: :job do
  include ActiveJob::TestHelper

  context "with compose deployment" do
    let(:updater_class) { Updater::Compose::Runner }
    let(:deploy_mode) { "compose" }

    it_behaves_like "updater job"
  end

  context "with k8s deployment" do
    let(:updater_class) { Updater::Kubernetes::Runner }
    let(:deploy_mode) { "k8s" }

    it_behaves_like "updater job"
  end
end
